ChangeLog

v0.3.0.0 2022/11/15
	(2022/11/15) PS1 - removed an unused import, and added a module
	declaration to keep Haddock happy.
	(2022/11/03) PS1 - many documentation fixes.
	(2022/11/03) PS1 - documented the need for -fno-cse.
	(2018/11/11) PS1 - replaced '.' by ':.' as the notation for
	abstraction. This solves two problems: (1) we now use the same
	notation for the constructor and the pattern, and (2) we don't
	need to hide (.) from the Prelude.
	(2018/11/11) PS1 - added more details on referential transparency
	and equivariance.
	(2018/11/10) PS1 - minor delinting, suggested by John Pavel.
	(2018/11/10) PS1 - added a type annotation to fix a compiler warning.
	(2018/11/10) PS1 - rearranged module dependencies and moved some
	instance declaration to avoid an orphaned instance declaration.
	(2018/11/10) PS1 - fixed spurious incomplete pattern match
	warnings for abstraction patterns.

v0.2.0.0 2018/11/10
	(2018/11/09) PS1 - documentation updates: added related works,
	acknowledgements, and references. Added a README file for
	bitbucket.
	(2018/11/09) PS1 - renamed Nominal.Generics as Nominal.Generic.
	(2018/11/09) PS1 - added type class instances for Ordering, Maybe,
	and Either.
	(2018/11/09) PS1 - renamed NominalPattern to NominalBinder, to
	avoid confusion between Haskell patterns and Nominal binders.
	(2018/11/09) PS1 - added abstraction patterns (:.).
	(2018/11/09) PS1 - removed unused open2 functions.

v0.1.0.0 2018/11/08
	Initial public release.
